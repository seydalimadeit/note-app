import {
  ADD_NOTE,
  SET_NOTES_FROM_STORE,
  UPDATE_NOTE,
  DELETE_NOTE,
  SET_NOTES_TO_LOCALSTORAGE
} from '~/constants/actions'
import { SET_NOTES, DELETE_NOTE_FROM_STORE, UPDATE_NOTE_IN_STORE } from '~/constants/mutations'
import { GET_NOTES } from '~/constants/getters'

export const state = () => ({
  notes: []
})

export const getters = {
  // GET ALL NOTES
  [GET_NOTES]: state => state.notes
}

export const mutations = {
  // SET NOTES TO STORE STATE
  [SET_NOTES] (store, notes) {
    store.notes = notes
  },
  // UPDATE NOTE IN STORE
  [UPDATE_NOTE_IN_STORE] (store, data) {
    store.notes[Number(data.index)] = data.note
  },
  // DELETE NOTE FROM STORE
  [DELETE_NOTE_FROM_STORE] (store, index) {
    store.notes.splice(index, 1)
  }
}

export const actions = {
  // SET NOTES ARRAY TO LOCALSTORAGE
  async [SET_NOTES_TO_LOCALSTORAGE] ({ state }) {
    await localStorage.setItem('notes', JSON.stringify(state.notes))
  },
  // ADD NOTE TO LOCALSTORAGE AND SET TO STORE
  async [ADD_NOTE] ({ commit, dispatch, state }, note) {
    await state.notes.push(note)

    await commit(SET_NOTES, state.notes)
    await dispatch(SET_NOTES_TO_LOCALSTORAGE)
  },
  // LOAD NOTES FROM LOCALSTORAGE TO STORE
  async [SET_NOTES_FROM_STORE] ({ commit }) {
    await commit(SET_NOTES, localStorage.getItem('notes') !== null ? Array.from(JSON.parse(localStorage.getItem('notes'))) : [])
  },
  // UPDATE NOTE IN STORE AND LOCALSTORAGE
  async [UPDATE_NOTE] ({ commit, state }, data) {
    await commit(UPDATE_NOTE_IN_STORE, data)
    await localStorage.setItem('notes', JSON.stringify(state.notes))
  },
  // DELETE NOTE FROM STORE AND LOCALSTORAGE
  async [DELETE_NOTE] ({ commit, dispatch }, index) {
    await commit(DELETE_NOTE_FROM_STORE, index)
    await dispatch(SET_NOTES_TO_LOCALSTORAGE)
  }
}
