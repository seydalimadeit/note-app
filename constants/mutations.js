/* NOTES */
export const SET_NOTES = 'SET_NOTES'
export const DELETE_NOTE_FROM_STORE = 'DELETE_NOTE_FROM_STORE'
export const UPDATE_NOTE_IN_STORE = 'UPDATE_NOTE_IN_STORE'
